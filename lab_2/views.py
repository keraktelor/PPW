from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Puji dan syukur penulis panjatkan kehadirat Allah SWT, karena berkat rahmat dan karunia-Nya, karya tulis ini dapat terselesaikan dengan baik. Shalawat serta salam tak lupa dicurahkan kepada Baginda Rasulullah SAW. Adapun tulisan yang berjudul “Tax Watch: Aplikasi Pembayaran, Pelacakan, dan Distribusi Pajak Berbasis Arsitektur Berorientasi Pelayanan dan Komputasi Awan” diajukan sebagai salah satu persyaratan dalam pemilihan Mahasiswa Berprestasi tingkat Fakultas Ilmu Komputer tahun 2018.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)

